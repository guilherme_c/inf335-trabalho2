package br.unicamp.ic.inf335.beans;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.net.URL;
import java.util.ArrayList;

import org.junit.Test;

public class AnuncioBeanTeste {

	@Test
	public void testCriacaoAnuncio() {
		ProdutoBean produto = new ProdutoBean("1", "Tênis Um", "Descrição do tênis um", 100.0, "Estado um");
		ArrayList<URL> fotos = new ArrayList<>();

		AnuncioBean anuncio = new AnuncioBean(produto, fotos, 10.0);

		assertEquals(anuncio.getProduto(), produto);
		assertEquals(anuncio.getFotosUrl(), fotos);
		assertEquals(anuncio.getDesconto(), 10.0);
	}

	@Test
	public void testValorAnuncioComDesconto() {
		ProdutoBean produto = new ProdutoBean("1", "Tênis Um", "Descrição do tênis um", 100.0, "Estado um");
		AnuncioBean anuncio = new AnuncioBean(produto, null, 10.0);

		assertEquals(anuncio.getValor(), 90.0);
	}

	@Test
	public void testValorAnuncioSemDesconto() {
		ProdutoBean produto = new ProdutoBean("1", "Tênis Um", "Descrição do tênis um", 100.0, "Estado um");
		AnuncioBean anuncio = new AnuncioBean(produto, null, 0.0);

		assertEquals(anuncio.getValor(), 100.0);
	}

	@Test
	public void testDescontoNegativo() {
		ProdutoBean produto = new ProdutoBean("1", "Tênis Um", "Descrição do tênis um", 100.0, "Estado um");

		assertThrows(IllegalArgumentException.class, () -> new AnuncioBean(produto, null, -10.0));
	}

	@Test
	public void testDescontMaiorQueValorProduto() {
		ProdutoBean produto = new ProdutoBean("1", "Tênis Um", "Descrição do tênis um", 100.0, "Estado um");

		assertThrows(IllegalArgumentException.class, () -> new AnuncioBean(produto, null, 120.0));
	}
}
